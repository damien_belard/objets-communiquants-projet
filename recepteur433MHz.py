
#!/usr/bin/env python
# -*- coding: latin-1 -*-

import time
import pigpio
import vw
import sys
import json
import datetime
from azure.servicebus import ServiceBusService
from decimal import Decimal

RX=11
BPS=2000
data = ""

HumidityTempDeviceId = "HTU21D_TempC"
PressureTempDeviceId = "MPL3115A2_TempF"
HumidityDeviceId = "MPL3115A2_Humidity"
LightDeviceId = "RedBoard_Light"
BatteryDeviceId = "RedBoard_Battery"

key_name ='RootManageSharedAccessKey'
key_value = 'ip/f3eeJgzaBnFEGbGxglQsfHkUijTdBDGR3/7Qr8bA='
sbs = ServiceBusService('station-meteo-ns',shared_access_key_name=key_name, shared_access_key_value=key_value)
  
pi = pigpio.pi() 
rx = vw.rx(pi, RX, BPS) 
start = time.time()

print("En attente de la reception des donnees")

while (time.time()-start) > -1:
   while rx.ready():
      for c in rx.get():
         data += chr (c)
      tempf, tempc, humidity, light, battery = data.split("#") 
      print "tempf: %s" % tempf
      print "tempc: %s" % tempc
      print "humidity: %s" % humidity
      print "light: %s" % light
      print "battery: %s" % battery
      print "---------------------------"
      print "-----AZURE Processing------"
      print "---------------------------"
      data = ""
      
      # Process Temperature C
      tempcBody = {'DeviceId': HumidityTempDeviceId, 'Time': str(datetime.datetime.now()), 'TemperatureC': tempc}
      print "sending tempc to azure srvicebus => " + tempc + " Celsius "
      sbs.send_event('tempc', json.dumps(tempcBody))
      
      # Process Temperature F
      tempfBody = {'DeviceId': PressureTempDeviceId, 'Time': str(datetime.datetime.now()), 'TemperatureF': tempf}
      print "sending tempf to azure srvicebus => " + tempf + " Fahrenheit"
      sbs.send_event('tempf', json.dumps(tempfBody))
      
      # Process Humidity
      humidityBody = {'DeviceId': HumidityDeviceId, 'Time': str(datetime.datetime.now()), 'Humidity': humidity}
      print "sending humidity to azure srvicebus => " + humidity + " %"
      sbs.send_event('humidity', json.dumps(humidityBody))
      
      # Process Light
      lightBody = {'DeviceId': LightDeviceId, 'Time': str(datetime.datetime.now()), 'Light': light}
      print "sending to azure srvicebus => " + light + " lux"
      sbs.send_event('light', json.dumps(lightBody))
      
      # Process Battery
      batteryBody = {'DeviceId': BatteryDeviceId, 'Time': str(datetime.datetime.now()), 'Battery': battery}
      print "sending to azure srvicebus => " + battery + " V"
      sbs.send_event('battery', json.dumps(batteryBody))
      
      print "-----------------------------------------------------------------------------------------------"
rx.cancel()
pi.stop()
